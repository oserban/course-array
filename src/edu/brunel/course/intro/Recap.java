package edu.brunel.course.intro;

public class Recap {
    public static void main(String[] args) {
        boolean condition = true;
        byte b = 112;
        int i = 465;
        long l = 0x345;
        float f = 3.14f;
        double d = 13.011;
        char c = 'x';
        String s = "main string";

        int x = i + i;
        long y = l * l;
    }
}
