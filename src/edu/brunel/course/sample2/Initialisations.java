package edu.brunel.course.sample2;

import java.util.Arrays;

public class Initialisations {
    public static void main(String[] args) {
        int n = 5;

        // standard array declaration
        int[] v = new int[n];
        v[0] = 1;
        v[1] = 3;
        v[2] = 5;
        v[3] = 7;
        v[4] = 11;
        System.out.println(Arrays.toString(v));

        v[2] = 35;
        System.out.println(Arrays.toString(v));

        // compact array declaration
        double[] dv = new double[]{3.14, 3.14};
        System.out.println(Arrays.toString(dv));

        boolean[] bv = new boolean[]{true, true, true, false};
        System.out.println(Arrays.toString(bv));

        String[] strings = new String[]{"1", "x", "y"};
        System.out.println(Arrays.toString(strings));
    }
}
