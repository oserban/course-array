package edu.brunel.course.sample2;

import java.util.Arrays;

public class Operations {
    public static void main(String[] args) {
        int n = 10;

        double[] dv1 = new double[n];
        Arrays.fill(dv1, 0); // fills the array with 0
        System.out.println(dv1); // prints the memory reference of the array, e.g. [D@85ede7b
        System.out.println(Arrays.toString(dv1)); // toString creates a representation of the content

        double[] dv2 = new double[n];
        Arrays.fill(dv2, 3.14); // fills the array with 3.14
        System.out.println(Arrays.toString(dv2));

        Arrays.fill(dv2, 0, 5, 0); // partially fills the array with 0
        System.out.println(Arrays.toString(dv2));

        System.out.println(Arrays.equals(dv1, dv2));

        dv2 = Arrays.copyOf(dv2, 100);
        Arrays.fill(dv2, 10, 100, 1);
        System.out.println(Arrays.toString(dv2));
    }
}
