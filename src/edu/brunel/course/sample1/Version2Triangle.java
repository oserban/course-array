package edu.brunel.course.sample1;

public class Version2Triangle {
    public static void main(String[] args) {
        int[] x = new int[3];
        x[0] = 11;
        x[1] = 24;
        x[2] = 35;

        int[] y = new int[3];
        y[0] = 11;
        y[1] = 24;
        y[2] = 13;

        int xO = (x[0] + x[1] + x[2]) / 3;
        int YO = (y[0] + y[1] + y[2]) / 3;
        System.out.println(xO); // -> 23
        System.out.println(YO); // -> 16
    }
}
