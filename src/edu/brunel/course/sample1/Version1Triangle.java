package edu.brunel.course.sample1;

public class Version1Triangle {
    public static void main(String[] args) {
        int xA = 11;
        int xB = 24;
        int xC = 35;

        int yA = 11;
        int yB = 24;
        int yC = 13;

        int xO = (xA + xB + xC) / 3;
        int YO = (yA + yB + yC) / 3;
        System.out.println(xO); // -> 23
        System.out.println(YO); // -> 16
    }
}
