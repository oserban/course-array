package edu.brunel.course.sample1;

public class Version3 {
    public static void main(String[] args) {
        int n = 5;

        // array declaration
        int[] x = new int[n];
        x[0] = 1;
        x[1] = 3;
        x[2] = 5;
        x[3] = 7;
        x[4] = 11;

        // alternative (compact) array declaration
        x = new int[]{1, 3, 5, 7, 11};

        int product = 1;
        // standard for loop
        for (int i = 0; i < x.length; i++) {
            product = product * x[i];
        }
        System.out.println(product); // -> 1155

        product = 1;
        // for each loop
        for (int element : x) {
            product = product * element;
        }
        System.out.println(product); // -> 1155
    }
}
