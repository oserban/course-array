package edu.brunel.course.sample3;

import java.util.Arrays;

public class Multidimensional {
    public static void main(String[] args) {
        int n = 4;
        int m = 5;

        double[][] matrix = new double[m][n];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = i * j;
            }
        }
        System.out.println(Arrays.deepToString(matrix));
        // -> [[0.0, 0.0, 0.0, 0.0], [0.0, 1.0, 2.0, 3.0], [0.0, 2.0, 4.0, 6.0], [0.0, 3.0, 6.0, 9.0], [0.0, 4.0, 8.0, 12.0]]


        for (double[] row : matrix) {
            Arrays.fill(row, 1.0);
        }
        System.out.println(Arrays.deepToString(matrix));
        // -> [[1.0, 1.0, 1.0, 1.0], [1.0, 1.0, 1.0, 1.0], [1.0, 1.0, 1.0, 1.0], [1.0, 1.0, 1.0, 1.0], [1.0, 1.0, 1.0, 1.0]]

        matrix = new double[m][];
        for (int i = 0; i < matrix.length; i++) {
            matrix[i] = new double[i + 1];

            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = i * j;
            }
        }
        System.out.println(Arrays.deepToString(matrix));
        // -> [[0.0], [0.0, 1.0], [0.0, 2.0, 4.0], [0.0, 3.0, 6.0, 9.0], [0.0, 4.0, 8.0, 12.0, 16.0]]
    }
}
