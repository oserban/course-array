package edu.brunel.course.sample3;

public class Pitfalls {
    public static void main(String[] args) {
        int[] v = new int[3];
        v[0] = 1;
        v[1] = 2;

        System.out.println(v[2]); // no error, but there is no guarantee on the returned value
        System.out.println(v[3]); // runtime error: ArrayIndexOutOfBoundsException exception
        v[3] = 4; // runtime error: ArrayIndexOutOfBoundsException exception

        System.out.println(v[v.length]); // runtime error: ArrayIndexOutOfBoundsException exception

        // v[1] = "test"; // compile time error: Incompatible types
    }
}
