# Course: Array

To run the code you need to compile the class first by executing in the **src/**
 folder:
 
```bash
javac edu/brunel/course/sample1/*
```

To execute:

```bash
java edu.brunel.course.sample1.Version1Triangle
```

